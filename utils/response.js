const statuses = require('../statuses');
const consts = require('../consts').RESPONSES;

/**
 *
 *
 * @class Response
 */
class Response {
  /**
   * Creates an instance of Response.
   * @param {any} res
   * @param {any} message
   * @param {any} status
   * @param {any} code
   * @param {any} type
   *
   * @memberOf Response
   */
  constructor(res, message, status, code, type) {
    this.status = status || statuses.OK;
    this.message = message || 'OK';
    this.code = code || consts.CODES.GENERIC;
    this.type = type || consts.TYPES.GENERIC;
    this.res = res;
    this.response = {
      message,
      code,
      type,
      response: res,
    };
  }
}

module.exports = Response;
