const codes = require('../consts').ERRORS.CODES;
const statuses = require('../statuses');

/**
 *
 *
 * @class RESTError
 * @extends {Error}
 */
class RESTError extends Error {
  /**
   * Creates an instance of RESTError.
   * @param {any} status
   * @param {any} args
   *
   * @memberOf RESTError
   */
  constructor(status, ...args) {
    let errors = [];
    //
    for (let index in args) {
      if ({}.hasOwnProperty.call(args, index)) {
        let messageObj = {
          message: args[index].message || 'INTERNAL SERVER ERROR',
          code: args[index].code || codes.GENERIC,
        };
        delete args[index].message;
        delete args[index].code;

        messageObj = {
          ...messageObj,
          ...args[index],
        };
        errors.push(messageObj);
      }
    }
    super(JSON.stringify({errors}));
    this.messageObj = {errors};
    this.status = status || statuses.INTERNAL;
    Error.captureStackTrace(this, RESTError);
  }
}

module.exports = RESTError;
