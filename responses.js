const Response = require('./utils/response');

const consts = require('./consts').RESPONSES;
const statuses = require('./statuses');

const resMessages = {
  hasBeenUpdate: (name) => `${name} has been updated`,
  hasBeenCreated: (name) => `${name} has been created`,
  hasBeenFound: (count, name) => `${count} ${name} has been found`,
  isFound: (name) => `${name} is found`,
};

const bookRes = (book, message, code) => {
  const privateBook = {
    _id: book._id,
    name: book.name,
    pageCount: book.pageCount,
  };
  const res = new Response(
    privateBook,
    message,
    statuses.OK,
    code,
    consts.TYPES.BOOK,
  );
  return {
    res: res.response,
    status: res.status,
  };
};

const booksRes = (books, message, code) => {
  let privateBooks = [];
  for (let index in books) {
    if ({}.hasOwnProperty.call(books, index)) {
      privateBooks.push({
        _id: books[index]._id,
        name: books[index].name,
        pageCount: books[index].pageCount,
      });
    }
  }
  const res = new Response(
    privateBooks,
    message,
    statuses.OK,
    code,
    consts.TYPES.BOOKS,
  );
  return {
    res: res.response,
    status: res.status,
  };
};

module.exports = {
  book: {
    update: (book) =>
      bookRes(
        book,
        resMessages.hasBeenUpdate(book.name),
        consts.CODES.BOOK_UPDATED,
      ),
    activate: (book) =>
      bookRes(
        book,
        resMessages.hasBeenUpdate(book.name),
        consts.CODES.BOOK_UPDATED,
      ),
    delete: (book) =>
      bookRes(
        book,
        resMessages.hasBeenUpdate(book.name),
        consts.CODES.BOOK_UPDATED,
      ),
    create: (book) =>
      bookRes(
        book,
        resMessages.hasBeenCreated(book.name),
        consts.CODES.BOOK_CREATED,
      ),
    list: (books) =>
      booksRes(
        books,
        resMessages.hasBeenFound(books.length, 'books'),
        consts.CODES.BOOKS_FOUND,
      ),
  },
};
