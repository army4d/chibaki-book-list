const app = require('express')();
const cors = require('cors');
const bodyParser = require('body-parser');
const logger = require('morgan');
const helmet = require('helmet');
const mongoose = require('mongoose');

const pathNotFound = require('./errors').pathNotFound;

const config = require('./config');

const port = process.env.PORT || config.port || 3000;

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(helmet());
app.use(cors());

app.use('/', require('./router'));

app.use(function(req, res, next) {
  next(pathNotFound('path that you were looking for'));
});

app.use(function(err, req, res, next) {
  console.log(err.stack);
  if (err.messageObj) {
    res.status(err.status).send(err.messageObj);
  } else if (err.status) {
    res.status(err.status).send(err.message);
  } else {
    res.status(500).send(err.message || err);
  }
});

mongoose.Promise = global.Promise;

mongoose.connect(
  config.db,
  {useNewUrlParser: true},
);

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('database connection successful with ' + config.db);
});

app.listen(port, function(err) {
  if (err) throw err;
  console.log('book-list RESTful api listening on port ' + config.port);
});
