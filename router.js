const express = require('express');
const router = new express.Router();

const book = require('./routes/book');

router.use('/book', book);

module.exports = router;
