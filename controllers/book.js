const bookModel = require('../models/book');
const consts = require('../consts').BOOK;

let dbCreateBook = (book) => bookModel.create(book);
let dbFindBooks = (condition, projection, option) =>
  new Promise((resolve, reject) =>
    bookModel.find(condition, projection, option, (err, res) => {
      if (err) return reject(err);
      resolve(res);
    }),
  );
let dbFindOneBook = (condition, projection, option) =>
  new Promise((resolve, reject) =>
    bookModel.findOne(condition, projection, option, (err, res) => {
      if (err) return reject(err);
      if (res == undefined) return reject('no book has been found');
      resolve(res);
    }),
  );
let dbFindBookById = (_id, projection, option) =>
  dbFindOneBook({_id}, projection, option);

let dbUpdateBook = (condition, update, option) =>
  new Promise((resolve, reject) =>
    bookModel.findOneAndUpdate(
      condition,
      update,
      {...option, runValidators: true},
      (err, book) => {
        if (err) return reject(err);
        let resp = {
          ...book._doc,
          ...update,
        };
        resolve(resp);
      },
    ),
  );

let createBook = (name, pageCount) => dbCreateBook({name, pageCount});

let updateBook = async (_id, name, pageCount) => {
  let book = await dbFindBookById(_id);

  let doc = {};
  if (name) doc['name'] = name;
  if (pageCount) doc['pageCount'] = pageCount;

  if (book.status == consts.STATUSES.DEACTIVE) {
    return Promise.reject('that book is deleted');
  }
  return await dbUpdateBook({_id}, doc);
};

let activateBook = async (_id) => {
  await dbFindBookById(_id);
  return await dbUpdateBook({_id}, {status: consts.STATUSES.ACTIVE});
};

let removeBook = async (_id) => {
  await dbFindBookById(_id);
  return await dbUpdateBook({_id}, {status: consts.STATUSES.DEACTIVE});
};

let listBooks = async (query) => {
  query = query || {};
  const conditions = {
    status: consts.STATUSES.ACTIVE,
  };
  const options = {};

  let order = -1;
  if (query.order) {
    if (query.order != 'DESC') order = 1;
  }
  if (query.sort_by_page_count == 'true') {
    options['sort'] = {
      pageCount: order,
    };
  } else {
    options['sort'] = {
      createdAt: order,
    };
  }

  let listPageCount = parseInt(query.list_page_count);
  let listPage = parseInt(query.list_page);

  if (listPage == NaN) listPage = 0;
  if (listPageCount == NaN) listPageCount = 0;

  const skip = listPage * listPageCount;
  const limit = listPageCount;

  options['skip'] = skip;
  if (limit != 0) {
    options['limit'] = limit;
  }

  return await dbFindBooks(conditions, null, options);
};

const bookController = {
  createBook,
  updateBook,
  activateBook,
  removeBook,
  listBooks,
};

module.exports = bookController;
