module.exports = {
  OK: 200,
  INTERNAL: 500,
  NOT_FOUND: 404,
  BAD_REQUEST: 400,
};
