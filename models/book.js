const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const consts = require('../consts').BOOK;

const bookSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  pageCount: {
    type: Number,
    required: true,
  },
  status: {
    type: String,
    enum: consts.STATUSES_ARR,
    default: consts.STATUSES.ACTIVE,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
});

const bookModel = mongoose.model('book', bookSchema);

module.exports = bookModel;
