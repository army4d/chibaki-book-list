const express = require('express');
const router = new express.Router();
const asyncWrapper = require('express-async-handler');

const bookController = require('../controllers/book');

const bookRes = require('../responses').book;

const errors = require('../errors');

const handlers = {
  createBook: asyncWrapper(async (req, res, next) => {
    let book;
    try {
      book = await bookController.createBook(req.body.name, req.body.pageCount);
    } catch (e) {
      if (e.name === 'MongoError' && e.code === 121) {
        return next(errors.badRequest('creating a book'));
      } else {
        return next(errors.internal());
      }
    }

    const resp = bookRes.create(book);
    return res.status(resp.status).send(resp.res);
  }),
  updateBook: asyncWrapper(async (req, res, next) => {
    let _id = req.params.id;
    let result;

    try {
      result = await bookController.updateBook(
        _id,
        req.body.name,
        req.body.pageCount,
      );
    } catch (e) {
      if (e.name === 'MongoError' && e.code === 121) {
        return next(errors.badRequest('updating a book'));
      } else if (typeof e == 'string') {
        return next(errors.badRequest('updating a book'));
      } else {
        return next(errors.internal());
      }
    }
    const resp = bookRes.update(result);
    return res.status(resp.status).send(resp.res);
  }),
  activateBook: asyncWrapper(async (req, res, next) => {
    let _id = req.params.id;
    let result;

    try {
      result = await bookController.activateBook(_id);
    } catch (e) {
      if (e.name === 'MongoError' && e.code === 121) {
        return next(errors.badRequest('activating a book'));
      } else if (typeof e == 'string') {
        return next(errors.badRequest('activating a book'));
      } else {
        return next(errors.internal());
      }
    }
    const resp = bookRes.activate(result);
    return res.status(resp.status).send(resp.res);
  }),
  removeBook: asyncWrapper(async (req, res, next) => {
    let _id = req.params.id;
    let result;

    try {
      result = await bookController.removeBook(_id);
    } catch (e) {
      if (e.name === 'MongoError' && e.code === 121) {
        return next(errors.badRequest('removing a book'));
      } else if (typeof e == 'string') {
        return next(errors.badRequest('removing a book'));
      } else {
        return next(errors.internal());
      }
    }
    const resp = bookRes.delete(result);
    return res.status(resp.status).send(resp.res);
  }),
  list: asyncWrapper(async (req, res, next) => {
    let results;

    try {
      results = await bookController.listBooks(req.query);
    } catch (e) {
      return next(errors.internal());
    }
    const resp = bookRes.list(results);
    return res.status(resp.status).send(resp.res);
  }),
};

router.post('/create', handlers.createBook);
router.put('/update/:id', handlers.updateBook);
router.put('/active/:id', handlers.activateBook);
router.delete('/remove/:id', handlers.removeBook);
router.get('/list', handlers.list);

module.exports = router;
