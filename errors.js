const RESTError = require('./utils/error');

const codes = require('./consts').ERRORS.CODES;
const statuses = require('./statuses');

const errorMessages = {
  wasNotFound: (name) => `${name} was NOT found`,
  badRequest: (req) => `your request for ${req} was not eligible`,
  noMatchFound: (req) => `no match found for ${req}`,
  internal: () => `internal server error`,
};

const internal = () => {
  let status = statuses.INTERNAL;
  return new RESTError(status, {
    message: errorMessages.internal(),
    code: codes.GENERIC,
  });
};

const notFound = (...args) => {
  let status = statuses.NOT_FOUND;
  let errs = [];
  for (let arg of args) {
    if (typeof arg == 'object') {
      errs.push(arg);
    } else if (typeof arg == 'string') {
      errs.push({
        message: errorMessages.wasNotFound(arg),
        code: codes.NOT_FOUND,
      });
    }
  }
  return new RESTError(status, ...errs);
};

const badRequest = (...args) => {
  let status = statuses.BAD_REQUEST;
  let errs = [];
  for (let arg of args) {
    if (typeof arg == 'object') {
      errs.push(arg);
    } else if (typeof arg == 'string') {
      errs.push({
        message: errorMessages.badRequest(arg),
        code: codes.BAD_REQUEST,
      });
    }
  }
  return new RESTError(status, ...errs);
};

const noMatchFound = (...args) => {
  let status = statuses.NOT_FOUND;
  let errs = [];
  for (let arg of args) {
    if (typeof arg == 'object') {
      errs.push(arg);
    } else if (typeof arg == 'string') {
      errs.push({
        message: errorMessages.noMatchFound(arg),
        code: codes.NOT_FOUND,
      });
    }
  }
  return new RESTError(status, ...errs);
};

module.exports = {
  internal: () => internal(),
  pathNotFound: (path) => notFound(path),
  badRequest: (req) => badRequest(req),
  noMatchFound: (req) => noMatchFound(req),
};
